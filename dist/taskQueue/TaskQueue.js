"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TaskQueue = /** @class */ (function () {
    function TaskQueue(tasks) {
        var _this = this;
        this.toComplete = {};
        this.completed = {};
        this.containsTask = function (task) { return typeof _this.toComplete[task] == 'boolean'; };
        this.addTask = function (task) { return _this.toComplete[task] = true; };
        this.removeTask = function (task) {
            delete _this.toComplete[task];
            delete _this.completed[task];
        };
        this.getTasksToComplete = function () { return _this.toComplete; };
        this.getCompleteTasks = function () { return _this.completed; };
        this.setTasks = function (tasks) { return _this.toComplete = tasks; };
        this.setCompleteTasks = function (tasks) {
            Object.keys(tasks).forEach(function (task) {
                if (!_this.toComplete[task])
                    throw new Error("Invalid element " + task);
                _this.completed[task] = true;
            });
        };
        if (Array.isArray(tasks)) {
            tasks.forEach(function (task) {
                if (_this.toComplete[task])
                    throw new Error("Duplicated task " + task + " in initial task collection");
                _this.toComplete[task] = true;
            });
        }
        else {
            if (typeof tasks == 'object' && Object.keys(tasks).length)
                this.toComplete = tasks;
        }
    }
    TaskQueue.prototype.completeTask = function (task, onAllDone) {
        if (!this.toComplete[task])
            throw new Error("Invalid task to process " + task);
        this.completed[task] = true;
        if (Object.keys(this.toComplete).length == Object.keys(this.completed).length)
            typeof onAllDone == 'function' && onAllDone(this.toComplete);
    };
    return TaskQueue;
}());
exports.default = TaskQueue;
