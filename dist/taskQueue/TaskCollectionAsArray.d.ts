export default interface TaskCollectionAsArray {
    [s: number]: string;
}
