import TaskCollectionAsMap from './TaskCollectionAsMap';
import TaskCollectionAsArray from './TaskCollectionAsArray';
export default class TaskQueue {
    private toComplete;
    private completed;
    constructor(tasks?: TaskCollectionAsMap | TaskCollectionAsArray);
    completeTask(task: string, onAllDone?: (completedTasks: TaskCollectionAsMap) => void): void;
    containsTask: (task: string) => boolean;
    addTask: (task: string) => boolean;
    removeTask: (task: string) => void;
    getTasksToComplete: () => TaskCollectionAsMap;
    getCompleteTasks: () => TaskCollectionAsMap;
    setTasks: (tasks: TaskCollectionAsMap) => TaskCollectionAsMap;
    setCompleteTasks: (tasks: TaskCollectionAsMap) => void;
}
