declare const gearTypes: {
    gearColor: boolean;
    gearDisplay: boolean;
    gearLook: boolean;
    gearSize: boolean;
    gearXY: boolean;
    gearText: boolean;
};
export default gearTypes;
