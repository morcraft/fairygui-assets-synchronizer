export interface Gears {
    colorGear: HTMLElement;
    lookGear: HTMLElement;
    sizeGear: HTMLElement;
    XYGear: HTMLElement;
    displayGear?: HTMLElement;
}
export default function addLanguageGears(componentDOM: Document, element: Element, logger: any, controllerName: string, imageSourcePath?: string): Gears;
