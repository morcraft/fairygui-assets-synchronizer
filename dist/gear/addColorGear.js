"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var attributeToGearValue_1 = require("./attributeToGearValue");
function addColorGear(componentDOM, element, controllerName) {
    var gear = componentDOM.createElement('gearColor');
    gear.setAttribute('controller', controllerName);
    var color = attributeToGearValue_1.setGearValue(element, 'color');
    gear.setAttribute('default', color);
    element.appendChild(gear);
    return gear;
}
exports.default = addColorGear;
