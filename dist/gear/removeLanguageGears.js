"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var getElementsByTagAndAttribute_1 = require("../dom/getElementsByTagAndAttribute");
var gearTypes_1 = require("./gearTypes");
function removeLanguageGears(element, controllerName) {
    Object.keys(gearTypes_1.default).forEach(function (gearType) {
        var displayElements = getElementsByTagAndAttribute_1.default(element, gearType, 'controller', controllerName);
        displayElements.forEach(function (element) {
            element.parentNode.removeChild(element);
        });
    });
}
exports.default = removeLanguageGears;
