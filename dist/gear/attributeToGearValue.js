"use strict";
//This module contains the default value implementations for FairyGUI gear elements
Object.defineProperty(exports, "__esModule", { value: true });
//GearLook = Alpha/Rotation/Grayed/Touchable
exports.alpha = '1';
exports.rotation = '0';
exports.grayed = '0';
exports.touchable = '1';
//GearSize = Width/Height/ScaleX/ScaleY
exports.size = '1,1';
exports.scale = '1,1';
//GearColor
exports.color = '#ffffff';
//GearXY
exports.xy = '1,1';
//GearText
exports.text = '';
exports.map = {
    alpha: exports.alpha, rotation: exports.rotation, grayed: exports.grayed, touchable: exports.touchable, size: exports.size, scale: exports.scale, color: exports.color, xy: exports.xy, text: exports.text
};
function setGearValue(element, attribute) {
    var value = element.getAttribute(attribute);
    //touchable/invisible/grayed
    if (value == 'true')
        return '1';
    if (value == 'false')
        return '0';
    if (typeof value == 'string' && value.length)
        return value;
    return exports.map[attribute];
}
exports.setGearValue = setGearValue;
