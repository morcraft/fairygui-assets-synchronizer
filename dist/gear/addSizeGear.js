"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var attributeToGearValue = require("./attributeToGearValue");
var imageSize = require("image-size");
function addSizeGear(componentDOM, element, controllerName, logger, imageSourcePath) {
    //we need imageSourcePath here, because if the image doesn't have a set width and height, FairyGUI will add the picture size
    //as a default value in the SizeGear, so we need to get the real value in pixels to set it in case
    //it doesn't have one as an attribute
    var gear = componentDOM.createElement('gearSize');
    gear.setAttribute('controller', controllerName);
    var size = element.getAttribute('size');
    if (typeof size != 'string' || !size.length) {
        try {
            if (element.tagName.toLowerCase() == 'image') {
                if (!imageSourcePath)
                    throw new Error("An image source path is needed for image node");
                var realSize = imageSize(imageSourcePath);
                size = realSize.width + "," + realSize.height;
            }
            else {
                size = attributeToGearValue.size;
            }
        }
        catch (error) {
            logger.error("Error while getting size of " + imageSourcePath + " " + error);
            throw error;
        }
    }
    var scale = attributeToGearValue.setGearValue(element, 'scale');
    gear.setAttribute('default', size + "," + scale);
    element.appendChild(gear);
    return gear;
}
exports.default = addSizeGear;
