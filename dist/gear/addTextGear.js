"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var attributeToGearValue_1 = require("./attributeToGearValue");
function addTextYear(componentDOM, element, controllerName) {
    var gear = componentDOM.createElement('gearText');
    gear.setAttribute('controller', controllerName);
    gear.setAttribute('default', attributeToGearValue_1.setGearValue(element, 'text'));
    element.appendChild(gear);
    return gear;
}
exports.default = addTextYear;
