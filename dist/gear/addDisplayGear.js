"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function addDisplayGear(componentDOM, element, controllerName) {
    var gear = componentDOM.createElement('gearDisplay');
    gear.setAttribute('controller', controllerName);
    gear.setAttribute('pages', '0');
    element.appendChild(gear);
    return gear;
}
exports.default = addDisplayGear;
