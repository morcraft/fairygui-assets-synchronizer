"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var attributeToGearValue_1 = require("./attributeToGearValue");
function addLookGear(componentDOM, element, controllerName) {
    var gear = componentDOM.createElement('gearLook');
    gear.setAttribute('controller', controllerName);
    var alpha = attributeToGearValue_1.setGearValue(element, 'alpha');
    var rotation = attributeToGearValue_1.setGearValue(element, 'rotation');
    var grayed = attributeToGearValue_1.setGearValue(element, 'grayed');
    var touchable = attributeToGearValue_1.setGearValue(element, 'touchable');
    gear.setAttribute('default', alpha + "," + rotation + "," + grayed + "," + touchable);
    element.appendChild(gear);
    return gear;
}
exports.default = addLookGear;
