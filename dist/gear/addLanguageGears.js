"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var addColorGear_1 = require("./addColorGear");
var addDisplayGear_1 = require("./addDisplayGear");
var addLookGear_1 = require("./addLookGear");
var addSizeGear_1 = require("./addSizeGear");
var addXYGear_1 = require("./addXYGear");
function addLanguageGears(componentDOM, element, logger, controllerName, imageSourcePath) {
    var gears = {
        colorGear: addColorGear_1.default(componentDOM, element, controllerName),
        lookGear: addLookGear_1.default(componentDOM, element, controllerName),
        sizeGear: addSizeGear_1.default(componentDOM, element, controllerName, logger, imageSourcePath),
        XYGear: addXYGear_1.default(componentDOM, element, controllerName),
    };
    //text elements' visibility shouldn't change according to the controller's page
    if (element.tagName.toLowerCase() != 'text') {
        gears.displayGear = addDisplayGear_1.default(componentDOM, element, controllerName);
    }
    return gears;
}
exports.default = addLanguageGears;
