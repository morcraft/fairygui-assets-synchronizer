"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var attributeToGearValue_1 = require("./attributeToGearValue");
function addGXYear(componentDOM, element, controllerName) {
    var gear = componentDOM.createElement('gearXY');
    gear.setAttribute('controller', controllerName);
    gear.setAttribute('default', attributeToGearValue_1.setGearValue(element, 'xy'));
    element.appendChild(gear);
    return gear;
}
exports.default = addGXYear;
