"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var gearTypes = {
    gearColor: true,
    gearDisplay: true,
    gearLook: true,
    gearSize: true,
    gearXY: true,
    gearText: true,
};
exports.default = gearTypes;
