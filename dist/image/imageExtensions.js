"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = [
    'tif',
    'tiff',
    'gif',
    'jpeg',
    'jpg',
    'jif',
    'jfif',
    'jp2',
    'jpx',
    'j2k',
    'j2c',
    'fpx',
    'pcd',
    'png',
    'pdf',
];
