import TaskQueue from '../taskQueue/TaskQueue';
import Dictionary from '../utils/Dictionary';
import TranslationMap from '../language/TranslationMap';
export interface FunctionArguments {
    image: Element;
    projectPath: string;
    packageDOM: Document;
    packagePath: string;
    componentDOM: Document;
    componentPath: string;
    assetsQueue: TaskQueue;
    languageSuffixMap: Dictionary;
    languageSuffixRegex: RegExp;
    translationMap: TranslationMap;
    imageSourcePath: string;
    controllerName: string;
    logger: any;
}
export default function processImage(c: FunctionArguments): void;
