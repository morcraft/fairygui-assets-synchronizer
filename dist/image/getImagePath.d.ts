export default function getImagePath(image: Element, imageInPackage: Element, packagePath: string, componentPath: string): string;
