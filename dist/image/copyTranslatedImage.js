"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var copyFileAndLog_1 = require("../file/copyFileAndLog");
function copyTranslatedImage(projectPath, imagesSourcePath, imagePath, imageFileName, assetsQueue, logger) {
    var originPath = path.join(imagesSourcePath, imageFileName);
    var targetPath = path.join(projectPath, imagePath, imageFileName);
    //we avoid copying the same asset more than once, 
    //since these may appear multiple times across components
    if (assetsQueue.containsTask(targetPath))
        return;
    assetsQueue.addTask(targetPath);
    return copyFileAndLog_1.default(originPath, targetPath, logger)
        .then(function (_) {
        assetsQueue.completeTask(targetPath);
    })
        .catch(function (error) {
        assetsQueue.completeTask(targetPath);
        throw error;
    });
}
exports.default = copyTranslatedImage;
