"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var getElementsByTagAndAttribute_1 = require("../dom/getElementsByTagAndAttribute");
function appendTranslatedImageNode(image, suffix, suffixIndex, fileNameAttr, imagePath, imageFileName, imageInPackage, controllerName) {
    //const newImage = DOM.createElement('image')
    var clonedImage = image.cloneNode(true);
    //these attributes should change since these pictures aren't defined in package.xml
    clonedImage.setAttribute('id', image.getAttribute('id') + "_" + suffix);
    clonedImage.setAttribute('name', image.getAttribute('name') + "_" + suffix);
    var targetGear = getElementsByTagAndAttribute_1.default(clonedImage, 'gearDisplay', 'controller', controllerName)[0];
    targetGear.setAttribute('pages', (suffixIndex + 1).toString());
    if (typeof fileNameAttr == 'string' && fileNameAttr.length)
        clonedImage.setAttribute('fileName', path.join(imagePath, imageFileName));
    if (imageInPackage) {
        clonedImage.setAttribute('src', image.getAttribute('src') + "_" + suffix);
        var clonedImageInPackage = imageInPackage.cloneNode(true);
        imageInPackage.parentNode.insertBefore(clonedImageInPackage, imageInPackage);
        clonedImageInPackage.setAttribute('id', imageInPackage.getAttribute('id') + "_" + suffix);
        clonedImageInPackage.setAttribute('name', imageFileName);
    }
    return image.parentNode.insertBefore(clonedImage, image);
}
exports.default = appendTranslatedImageNode;
