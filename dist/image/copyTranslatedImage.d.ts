import TaskQueue from '../taskQueue/TaskQueue';
export default function copyTranslatedImage(projectPath: string, imagesSourcePath: string, imagePath: string, imageFileName: string, assetsQueue: TaskQueue, logger: any): Promise<void>;
