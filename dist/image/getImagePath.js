"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
function getImagePath(image, imageInPackage, packagePath, componentPath) {
    var srcAttr = image.getAttribute('src');
    var fileNameAttr = image.getAttribute('fileName');
    var fileName;
    //we check the fileName attribute first, since FairyGUI gives it a higher priority over the src attribute, apparently...
    if (typeof fileNameAttr != 'string' || !fileNameAttr.length) {
        if (typeof srcAttr != 'string' || !srcAttr.length)
            throw new Error("Image in " + componentPath + " doesn't have a valid 'fileName' or 'src' attribute to match against " + packagePath + ".");
        if (!imageInPackage)
            throw new Error("Impossible to find a valid source for image with invalid 'fileName' attribute and src '" + srcAttr + "' in '" + componentPath + "'. Non-existent element in package.xml");
        var _fileName = imageInPackage.getAttribute('name');
        var _path = imageInPackage.getAttribute('path');
        fileName = path.join(_path, _fileName);
    }
    else {
        fileName = fileNameAttr;
    }
    return fileName;
}
exports.default = getImagePath;
