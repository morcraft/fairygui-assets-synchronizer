"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var getElementsByTagAndAttribute_1 = require("../dom/getElementsByTagAndAttribute");
var getImagePath_1 = require("./getImagePath");
var chalk = require('chalk');
var path = require("path");
var removeLanguageGears_1 = require("../gear/removeLanguageGears");
var appendTranslatedImageNode_1 = require("./appendTranslatedImageNode");
var copyTranslatedImage_1 = require("./copyTranslatedImage");
var addDisplayGear_1 = require("../gear/addDisplayGear");
function processImage(c) {
    var fileName;
    var srcAttr = c.image.getAttribute('src');
    var fileNameAttr = c.image.getAttribute('fileName');
    var imageInPackage = getElementsByTagAndAttribute_1.default(c.packageDOM, 'image', 'id', srcAttr)[0];
    try {
        fileName = getImagePath_1.default(c.image, imageInPackage, c.packagePath, c.componentPath);
    }
    catch (error) {
        c.logger.error(chalk.red(error));
        throw error;
    }
    // if(typeof srcAttr == 'string'){
    //TODO send verbose mode to show this
    //if(!imageInPackage)
    //logger.warn(chalk.yellow(`${packagePath} doesn't contain any element with id '${srcAttr}' referenced in ${componentPath}. This can cause misleading references. Fix manually. Using attribute 'fileName' instead.`))
    // }
    var baseName = path.basename(fileName);
    var matches = baseName.match(c.languageSuffixRegex);
    var suffixLessFileName = baseName.replace(c.languageSuffixRegex, '');
    var imagePath = path.dirname(fileName);
    if (matches) {
        //we enter this block only if the picture has a language suffix
        //and exists in the translation map
        if (c.translationMap[suffixLessFileName]) {
            //we remove it here, since it might have outdated data and we'll add it later 
            //with different attributes anyway
            c.image.parentNode.removeChild(c.image);
            //if it's referenced via 'src' attribute, we remove it from the package definition
            if (imageInPackage)
                imageInPackage.parentNode.removeChild(imageInPackage);
        }
        //we return, since it contains a language prefix
        //and there's no need to process it
        return;
    }
    //logger.info(translationMap[suffixLessFileName])
    //if this image doesn't need to be translated, we return
    if (!c.translationMap[suffixLessFileName])
        return;
    removeLanguageGears_1.default(c.image, c.controllerName);
    addDisplayGear_1.default(c.componentDOM, c.image, c.controllerName);
    copyTranslatedImage_1.default(c.projectPath, c.imageSourcePath, imagePath, path.basename(fileName), c.assetsQueue, c.logger);
    Object.keys(c.languageSuffixMap).forEach(function (suffix, suffixIndex) {
        var imageFileName = c.translationMap[suffixLessFileName]["_" + suffix];
        if (!imageFileName)
            throw new Error("Missing '" + suffix + "' path for '" + suffixLessFileName + "' in translation map");
        appendTranslatedImageNode_1.default(c.image, suffix, suffixIndex, fileNameAttr, imagePath, imageFileName, imageInPackage, c.controllerName);
        copyTranslatedImage_1.default(c.projectPath, c.imageSourcePath, imagePath, imageFileName, c.assetsQueue, c.logger);
    });
}
exports.default = processImage;
