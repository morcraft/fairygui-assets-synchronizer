"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var escapeSpecialCharacters_1 = require("../language/escapeSpecialCharacters");
function sanitizeDictionary(dictionary) {
    var map = {};
    Object.keys(dictionary).forEach(function (key) {
        map[escapeSpecialCharacters_1.default(key)] = dictionary[key];
    });
    return map;
}
exports.default = sanitizeDictionary;
