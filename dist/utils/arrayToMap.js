"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function arrayToMap(array) {
    var map = {};
    array.forEach(function (element) {
        if (map[element])
            throw new Error("Key " + element + " appears more than once in the collection.");
        map[element] = element;
    });
    return map;
}
exports.default = arrayToMap;
