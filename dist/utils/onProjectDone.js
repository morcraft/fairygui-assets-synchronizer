"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chalk = require('chalk');
var pretty = require('pretty');
var fs = require("fs-extra");
var index_1 = require("../index");
function onProjectDone(packagePath, packageDOM, logger) {
    logger.info(chalk.green("Finished processing " + packagePath));
    var packageString = index_1.XMLSerializer.serializeToString(packageDOM);
    fs.outputFile(packagePath, pretty(packageString))
        .then(function (_) {
        logger.info(chalk.bgGreen(packagePath) + " \u2714");
    })
        .catch(function (error) {
        logger.error(chalk.red(error));
        throw error;
    });
}
exports.default = onProjectDone;
