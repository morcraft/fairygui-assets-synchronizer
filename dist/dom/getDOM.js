"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getDOM(parser, buffer) {
    return parser.parseFromString(buffer.toString());
}
exports.default = getDOM;
