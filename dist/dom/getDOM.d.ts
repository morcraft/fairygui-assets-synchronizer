/// <reference types="node" />
import xmldom = require('xmldom');
export default function getDOM(parser: xmldom.DOMParser, buffer: Buffer): Document;
