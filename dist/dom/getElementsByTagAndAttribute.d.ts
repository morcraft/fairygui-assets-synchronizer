export default function queryElementsByTagAndAttribute(DOM: Document | Element, tagName: string, attribute: string, value: string): Element[];
