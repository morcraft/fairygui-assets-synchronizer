"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function queryElementsByTagAndAttribute(DOM, tagName, attribute, value) {
    var elements = [];
    var queried = DOM.getElementsByTagName(tagName);
    for (var i = 0; i < queried.length; i++) {
        var attr = queried[i].getAttribute(attribute);
        if (attr == value) {
            elements.push(queried[i]);
        }
    }
    return elements;
}
exports.default = queryElementsByTagAndAttribute;
