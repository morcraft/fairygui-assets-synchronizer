"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var readFileAndLog_1 = require("../file/readFileAndLog");
var getDOM_1 = require("../dom/getDOM");
var index_1 = require("../index");
var getComponentChildren_1 = require("./getComponentChildren");
var processImage_1 = require("../image/processImage");
var addLanguageController_1 = require("../language/addLanguageController");
var onProjectDone_1 = require("../utils/onProjectDone");
var writeFileAndLog_1 = require("../file/writeFileAndLog");
function processComponent(c) {
    c.componentsQueue.addTask(c.componentPath);
    return readFileAndLog_1.default(c.componentPath, c.logger)
        .then(function (componentBuffer) {
        var componentDOM = getDOM_1.default(index_1.DOMParser, componentBuffer);
        var componentChildren = getComponentChildren_1.default(componentDOM);
        if (!Array.isArray(componentChildren) || !componentChildren.length) {
            c.componentsQueue.completeTask(c.componentPath, (function (_) { onProjectDone_1.default(c.packagePath, c.packageDOM, c.logger); }));
            return true;
        }
        var component = componentChildren[0];
        var images = component.getElementsByTagName('image');
        var imagesLength = images.length; //don't even attempt to remove this
        for (var i = 0; i < imagesLength; i++) {
            var image = images[i];
            processImage_1.default({
                image: image,
                projectPath: c.projectPath,
                packageDOM: c.packageDOM,
                packagePath: c.packagePath,
                componentDOM: componentDOM,
                componentPath: c.componentPath,
                assetsQueue: c.assetsQueue,
                languageSuffixMap: c.languageSuffixMap,
                languageSuffixRegex: c.languageSuffixRegex,
                translationMap: c.translationMap,
                imageSourcePath: c.imageSourcePath,
                controllerName: c.controllerName,
                logger: c.logger,
            });
        }
        addLanguageController_1.default(componentDOM, component, c.languageSuffixes, c.controllerName);
        c.componentsQueue.completeTask(c.componentPath, (function (_) { onProjectDone_1.default(c.packagePath, c.packageDOM, c.logger); }));
        var componentXMLString = index_1.XMLSerializer.serializeToString(componentDOM);
        writeFileAndLog_1.default(c.componentPath, componentXMLString, c.logger);
    });
}
exports.default = processComponent;
