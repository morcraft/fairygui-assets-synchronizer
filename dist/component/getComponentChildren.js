"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getComponentChildren(component) {
    var nodes = [];
    for (var i = 0; i < component.childNodes.length; i++) {
        if (component.childNodes[i].nodeName == 'component')
            nodes.push(component.childNodes[i]);
    }
    return nodes;
}
exports.default = getComponentChildren;
