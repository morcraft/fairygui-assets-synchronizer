import Dictionary from '../utils/Dictionary';
import TaskQueue from '../taskQueue/TaskQueue';
import TranslationMap from '../language/TranslationMap';
export interface FunctionArguments {
    projectPath: string;
    componentPath: string;
    packagePath: string;
    packageDOM: Document;
    componentsQueue: TaskQueue;
    assetsQueue: TaskQueue;
    languageSuffixMap: Dictionary;
    languageSuffixes: string[];
    languageSuffixRegex: RegExp;
    translationMap: TranslationMap;
    imageSourcePath: string;
    controllerName: string;
    logger: any;
}
export default function processComponent(c: FunctionArguments): Promise<boolean>;
