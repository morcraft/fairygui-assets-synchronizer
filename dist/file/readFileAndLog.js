"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs-extra");
function readFileAndLog(filePath, logger) {
    return fs.readFile(filePath)
        .catch(function (error) {
        logger.error("Error while reading file " + filePath, error);
        throw error;
    });
}
exports.default = readFileAndLog;
