export default function getFileNameWithoutExtension(filePath: string): string;
