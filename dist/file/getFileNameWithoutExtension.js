"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var getFileExtension_1 = require("./getFileExtension");
function getFileNameWithoutExtension(filePath) {
    var extension = getFileExtension_1.default(filePath);
    if (extension.length)
        return filePath.split("." + extension).slice(0, -1).join('');
    return filePath;
}
exports.default = getFileNameWithoutExtension;
