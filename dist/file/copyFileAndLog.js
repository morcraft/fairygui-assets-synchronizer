"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chalk = require('chalk');
var fs = require("fs-extra");
function copyFileAndLog(originPath, targetPath, logger) {
    return fs.copy(originPath, targetPath)
        .then(function (_) {
        logger.info(chalk.green(originPath) + " => " + chalk.green(targetPath) + " \u2714");
    })
        .catch(function (error) {
        logger.error(chalk.red(originPath) + " => " + chalk.red(targetPath), error);
    });
}
exports.default = copyFileAndLog;
