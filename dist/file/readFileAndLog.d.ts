/// <reference types="node" />
export default function readFileAndLog(filePath: string, logger: any): Promise<Buffer>;
