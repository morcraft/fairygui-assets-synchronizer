"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var getFileExtension_1 = require("./getFileExtension");
function filterFiles(assets, extensions) {
    var images = [];
    assets.forEach(function (asset) {
        var extension = getFileExtension_1.default(asset);
        if (extensions.includes(extension))
            images.push(asset);
    });
    return images;
}
exports.default = filterFiles;
