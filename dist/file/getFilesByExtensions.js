"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var recursiveReadDir = require("recursive-readdir");
var filterFiles_1 = require("./filterFiles");
function getFilesByExtensions(directory, extensions) {
    var content = recursiveReadDir(directory);
    return content.then(function (files) {
        return filterFiles_1.default(files, extensions);
    }, function (error) {
        throw error;
    });
}
exports.default = getFilesByExtensions;
