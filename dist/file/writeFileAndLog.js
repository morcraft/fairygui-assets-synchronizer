"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chalk = require('chalk');
var fs = require("fs-extra");
function writeFileAndLog(targetPath, string, logger) {
    return fs.outputFile(targetPath, string)
        .then(function (_) {
        logger.info(chalk.cyan(targetPath) + " \u2714");
    })
        .catch(function (error) {
        logger.error("Error while writing file " + targetPath + " " + chalk.red(error));
        throw error;
    });
}
exports.default = writeFileAndLog;
