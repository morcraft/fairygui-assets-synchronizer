export default function getFilesByExtensions(directory: string, extensions: string[]): Promise<string[]>;
