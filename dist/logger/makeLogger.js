"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var simpleNodeLogger = require('simple-node-logger');
function makeLogger(options) {
    return simpleNodeLogger.createSimpleLogger(options);
}
exports.default = makeLogger;
