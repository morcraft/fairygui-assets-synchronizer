"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var loggerOptions = {
    logFilePath: path.join(process.cwd(), "fairyGUIProject" + Date.now() + ".log"),
    timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
};
exports.default = loggerOptions;
