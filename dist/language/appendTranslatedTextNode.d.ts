export default function appendTranslatedTextNode(text: Element, suffix: string, suffixIndex: number, controllerName: string): Element;
