"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getImagesTranslationMap(images, regex) {
    var map = {};
    images.forEach(function (image) {
        var _a;
        var matches = image.match(regex);
        if (!matches)
            return true;
        var key = image.replace(matches[0], '');
        if (!map[key]) {
            map[key] = (_a = {},
                _a[matches[0]] = image,
                _a);
        }
        else {
            map[key][matches[0]] = image;
        }
    });
    return map;
}
exports.default = getImagesTranslationMap;
