"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var getElementsByTagAndAttribute_1 = require("../dom/getElementsByTagAndAttribute");
function appendTranslatedTextNode(text, suffix, suffixIndex, controllerName) {
    var clonedText = text.cloneNode(true);
    clonedText.setAttribute('id', text.getAttribute('id') + "_" + suffix);
    clonedText.setAttribute('name', text.getAttribute('name') + "_" + suffix);
    var targetGear = getElementsByTagAndAttribute_1.default(clonedText, 'gearDisplay', 'controller', controllerName)[0];
    targetGear.setAttribute('pages', (suffixIndex + 1).toString());
    text.parentNode.insertBefore(clonedText, text);
    return clonedText;
}
exports.default = appendTranslatedTextNode;
