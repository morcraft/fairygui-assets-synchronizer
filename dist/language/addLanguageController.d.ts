export default function addLanguageController(componentDOM: Document, component: Element, languageList: string[], controllerName: string): HTMLElement;
