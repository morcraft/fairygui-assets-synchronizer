declare const languageMap: {
    TraditionalChinese: string;
    English: string;
    Indonesian: string;
    Japanese: string;
    Korean: string;
    Thai: string;
    Vietnamese: string;
};
export default languageMap;
