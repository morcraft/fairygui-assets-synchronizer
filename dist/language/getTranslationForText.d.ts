import Dictionary from '../utils/Dictionary';
import DictionaryCollection from '../utils/DictionaryCollection';
export default function getTranslationForText(text: string, dictionaryCollection: DictionaryCollection, languageMap: Dictionary): Dictionary;
