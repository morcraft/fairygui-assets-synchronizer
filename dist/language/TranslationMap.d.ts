export default interface TranslationMap {
    [s: string]: {
        [s: string]: string;
    };
}
