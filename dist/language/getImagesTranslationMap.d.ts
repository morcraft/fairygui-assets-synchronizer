import TranslationMap from './TranslationMap';
export default function getImagesTranslationMap(images: string[], regex: RegExp): TranslationMap;
