declare const languageSuffixMap: {
    CN2: string;
    EN: string;
    IN: string;
    JP: string;
    KR: string;
    TH: string;
    VN: string;
};
export default languageSuffixMap;
