"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var languageMap = {
    TraditionalChinese: 'CN2',
    English: 'EN',
    Indonesian: 'IN',
    Japanese: 'JP',
    Korean: 'KR',
    Thai: 'TH',
    Vietnamese: 'VN',
};
exports.default = languageMap;
