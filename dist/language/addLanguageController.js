"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var getElementsByTagAndAttribute_1 = require("../dom/getElementsByTagAndAttribute");
var mapToControllerValues_1 = require("../controller/mapToControllerValues");
function addLanguageController(componentDOM, component, languageList, controllerName) {
    var controllers = getElementsByTagAndAttribute_1.default(componentDOM, 'controller', 'name', controllerName);
    controllers.forEach(function (controller) {
        controller.parentNode.removeChild(controller);
    });
    var languageControllerValues = mapToControllerValues_1.default(languageList);
    var languageController = componentDOM.createElement('controller');
    languageController.setAttribute('name', controllerName);
    languageController.setAttribute('selected', '0');
    languageController.setAttribute('pages', languageControllerValues);
    //equivalent of prepend(), that is, to insert in the first position
    return component.insertBefore(languageController, component.firstChild);
}
exports.default = addLanguageController;
