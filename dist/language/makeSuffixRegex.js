"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function default_1(suffixes) {
    return new RegExp("_(" + suffixes.join('|') + ")");
}
exports.default = default_1;
