"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getTranslationDifferences(imagesToPlace, languageSuffixMap, translationMap, regex) {
    var filesToCheck = {};
    Object.keys(imagesToPlace).forEach(function (image) {
        if (!regex.test(image)) { //doesn't contain any language suffix
            if (!translationMap[image])
                filesToCheck[image] = [];
        }
    });
    Object.keys(translationMap).forEach(function (key) {
        var missingTranslations = {};
        Object.keys(languageSuffixMap).forEach(function (suffix) {
            if (!translationMap[key]["_" + suffix])
                missingTranslations["_" + suffix] = true;
        });
        var keys = Object.keys(missingTranslations);
        if (keys.length) {
            filesToCheck[key] = keys;
        }
    });
    return filesToCheck;
}
exports.default = getTranslationDifferences;
