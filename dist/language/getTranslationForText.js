"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var escapeSpecialCharacters_1 = require("./escapeSpecialCharacters");
function getTranslationForText(text, dictionaryCollection, languageMap) {
    var translations = {};
    var dictionaryKeys = Object.keys(dictionaryCollection);
    dictionaryKeys.forEach(function (dictionaryName) {
        var languageSuffix = languageMap[dictionaryName];
        if (typeof languageSuffix != 'string')
            throw new Error("Couldn't find suffix for language " + dictionaryName);
        var dictionary = dictionaryCollection[dictionaryName];
        if (!dictionary.hasOwnProperty(text))
            return true;
        if (typeof dictionary[text] != 'string')
            throw new Error("Invalid type for text " + text + " in " + dictionaryName + " dictionary. Type: " + typeof dictionary[text] + ". Text: " + text);
        translations[languageSuffix] = escapeSpecialCharacters_1.default(dictionary[text]);
    });
    var translationKeys = Object.keys(translations);
    //at least one translation is available
    if (translationKeys.length) {
        if (translationKeys.length != dictionaryKeys.length)
            throw new Error("Invalid translation sources. Text " + text + " doesn't appear in all the given dictionaries. It appears in " + JSON.stringify(translationKeys));
    }
    return translations;
}
exports.default = getTranslationForText;
