"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var languageSuffixMap = {
    CN2: 'Traditional Chinese',
    EN: 'English',
    IN: 'Indonesian',
    JP: 'Japanese',
    KR: 'Korean',
    TH: 'Thai',
    VN: 'Vietnamese',
};
exports.default = languageSuffixMap;
