"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function escapeSpecialCharacters(string) {
    if (typeof string != 'string')
        throw new Error("Invalid type for given string " + string);
    return string.replace(/\r/g, '');
}
exports.default = escapeSpecialCharacters;
