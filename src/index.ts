import xmldom = require('xmldom')
import addColorGear from './gear/addColorGear'
import addDisplayGear from './gear/addDisplayGear'
import addLookGear from './gear/addLookGear'
import addSizeGear from './gear/addSizeGear'
import addXYGear from './gear/addXYGear'
import addTextGear from './gear/addTextGear'
import * as attributeToGearValue from './gear/attributeToGearValue'
import addLanguageController from './language/addLanguageController'
import addLanguageGears from './gear/addLanguageGears'
import appendTranslatedImageNode from './image/appendTranslatedImageNode'
import appendTranslatedTextNode from './language/appendTranslatedTextNode'
import arrayToMap from './utils/arrayToMap'
import componentExtensions from './component/componentExtensions'
import copyFileAndLog from './file/copyFileAndLog'
import copyTranslatedImage from './image/copyTranslatedImage'
import Dictionary from './utils/Dictionary'
import DictionaryCollection from './utils/DictionaryCollection'
import filterFiles from './file/filterFiles'
import getComponentChildren from './component/getComponentChildren'
import getDOM from './dom/getDOM'
import getElementsByTagAndAttribute from './dom/getElementsByTagAndAttribute'
import getFileExtension from './file/getFileExtension'
import getFileNameWithoutExtension from './file/getFileNameWithoutExtension'
import getFilesByExtensions from './file/getFilesByExtensions'
import getImagePath from './image/getImagePath'
import getImagesTranslationMap from './language/getImagesTranslationMap'
import getTranslationDifferences from './language/getTranslationDifferences'
import getTranslationForText from './language/getTranslationForText'
import isChineseString from './language/isChineseString'
import gearTypes from './gear/gearTypes'
import languageSuffixMap from './language/languageSuffixMap'
import languageMap from './language/languageMap'
import makeSuffixRegex from './language/makeSuffixRegex'
import loggerOptions from './logger/loggerOptions'
import makeLogger from './logger/makeLogger'
import mapToControllerValues from './controller/mapToControllerValues'
import onProjectDone from './utils/onProjectDone'
import imageExtensions from './image/imageExtensions'
import processImage from './image/processImage'
import processComponent from './component/processComponent'
import readFileAndLog from './file/readFileAndLog'
import removeLanguageGears from './gear/removeLanguageGears'
import TaskCollectionAsMap from './taskQueue/TaskCollectionAsMap'
import TaskQueue from './taskQueue/TaskQueue'
import TranslationMap from './language/TranslationMap'
import writeFileAndLog from './file/writeFileAndLog'
import escapeSpecialCharacters from './language/escapeSpecialCharacters'
import sanitizeDictionary from './utils/sanitizeDictionary'

export const simpleNodeLogger = require('simple-node-logger')//typings? :c
export const DOMParser = new xmldom.DOMParser
export const XMLSerializer = new xmldom.XMLSerializer

export { 
    addLanguageController, addLanguageGears, appendTranslatedImageNode, appendTranslatedTextNode, 
    copyFileAndLog, copyTranslatedImage, getImagePath, readFileAndLog, arrayToMap, componentExtensions, 
    Dictionary, filterFiles, getComponentChildren, getDOM, getElementsByTagAndAttribute, getFileExtension, 
    getFileNameWithoutExtension, getFilesByExtensions, getImagesTranslationMap, getTranslationDifferences, 
    languageSuffixMap, languageMap, makeSuffixRegex, loggerOptions, makeLogger, mapToControllerValues, 
    onProjectDone, imageExtensions, TaskCollectionAsMap, TaskQueue, removeLanguageGears, writeFileAndLog, 
    processImage, processComponent, TranslationMap, addColorGear, addDisplayGear, addLookGear, 
    addSizeGear, addXYGear, addTextGear, attributeToGearValue, gearTypes, 
    DictionaryCollection, getTranslationForText, escapeSpecialCharacters,
    sanitizeDictionary, isChineseString
}