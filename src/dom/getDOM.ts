import xmldom = require('xmldom')

export default function getDOM(parser: xmldom.DOMParser, buffer: Buffer){
    return parser.parseFromString(buffer.toString())
}