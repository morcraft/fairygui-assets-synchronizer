export default function queryElementsByTagAndAttribute(DOM: Document | Element, tagName:string, attribute: string, value: string): Element[]{
    const elements = []
    const queried = DOM.getElementsByTagName(tagName)
    for(let i = 0; i < queried.length; i++){
        const attr = queried[i].getAttribute(attribute)
        if(attr == value){
            elements.push(queried[i])
        }
    }

    return elements
}