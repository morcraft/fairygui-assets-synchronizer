export default interface TaskCollectionAsMap{
    [s: string]: boolean
}