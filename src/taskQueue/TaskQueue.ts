import TaskCollectionAsMap from './TaskCollectionAsMap'
import TaskCollectionAsArray from './TaskCollectionAsArray'

export default class TaskQueue{
    private toComplete: TaskCollectionAsMap = {}
    private completed: TaskCollectionAsMap = {}
    constructor(tasks?: TaskCollectionAsMap | TaskCollectionAsArray){
        if(Array.isArray(tasks)){
            tasks.forEach(task => {
                if(this.toComplete[task])
                    throw new Error(`Duplicated task ${task} in initial task collection`)
                this.toComplete[task] = true
            })
        }
        else{
            if(typeof tasks == 'object' && Object.keys(tasks).length)
                this.toComplete = tasks as TaskCollectionAsMap
        }
    }

    completeTask(task: string, onAllDone?: (completedTasks: TaskCollectionAsMap) => void){
        if(!this.toComplete[task])
            throw new Error(`Invalid task to process ${task}`)

        this.completed[task] = true

        if(Object.keys(this.toComplete).length == Object.keys(this.completed).length)
            typeof onAllDone == 'function' && onAllDone(this.toComplete)
    }

    containsTask = (task: string) => typeof this.toComplete[task] == 'boolean'

    addTask = (task: string) => this.toComplete[task] = true

    removeTask = (task: string) => {
        delete this.toComplete[task]
        delete this.completed[task]
    }

    getTasksToComplete = () => this.toComplete
    getCompleteTasks = () => this.completed
    setTasks = (tasks: TaskCollectionAsMap) => this.toComplete = tasks
    setCompleteTasks = (tasks: TaskCollectionAsMap) => {
        Object.keys(tasks).forEach(task => {
            if(!this.toComplete[task])
                throw new Error(`Invalid element ${task}`)

            this.completed[task] = true
        })
    }
}