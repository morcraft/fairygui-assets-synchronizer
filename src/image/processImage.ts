import getElementsByTagAndAttribute from '../dom/getElementsByTagAndAttribute'
import TaskQueue from '../taskQueue/TaskQueue'
import Dictionary from '../utils/Dictionary'
import getImagePath from './getImagePath'
const chalk = require('chalk')
import path = require('path')
import removeLanguageGears from '../gear/removeLanguageGears'
import appendTranslatedImageNode from './appendTranslatedImageNode'
import copyTranslatedImage from './copyTranslatedImage'
import addDisplayGear from '../gear/addDisplayGear'
import TranslationMap from '../language/TranslationMap'

export interface FunctionArguments{
    image: Element
    projectPath: string
    packageDOM: Document
    packagePath: string
    componentDOM: Document
    componentPath: string
    assetsQueue: TaskQueue
    languageSuffixMap: Dictionary
    languageSuffixRegex: RegExp
    translationMap: TranslationMap
    imageSourcePath: string
    controllerName: string
    logger
}

export default function processImage(c: FunctionArguments){
    let fileName: string
    const srcAttr = c.image.getAttribute('src')
    const fileNameAttr = c.image.getAttribute('fileName')
    const imageInPackage = getElementsByTagAndAttribute(c.packageDOM, 'image', 'id', srcAttr)[0]

    try{
        fileName = getImagePath(c.image, imageInPackage, c.packagePath, c.componentPath)
    }
    catch(error){
        c.logger.error(chalk.red(error))
        throw error
    }

    // if(typeof srcAttr == 'string'){
        //TODO send verbose mode to show this
        //if(!imageInPackage)
            //logger.warn(chalk.yellow(`${packagePath} doesn't contain any element with id '${srcAttr}' referenced in ${componentPath}. This can cause misleading references. Fix manually. Using attribute 'fileName' instead.`))
    // }

    const baseName = path.basename(fileName)
    const matches = baseName.match(c.languageSuffixRegex)
    const suffixLessFileName = baseName.replace(c.languageSuffixRegex, '')
    const imagePath = path.dirname(fileName)

    if(matches){
        //we enter this block only if the picture has a language suffix
        //and exists in the translation map
        if(c.translationMap[suffixLessFileName]){
            //we remove it here, since it might have outdated data and we'll add it later 
            //with different attributes anyway
            c.image.parentNode.removeChild(c.image)

            //if it's referenced via 'src' attribute, we remove it from the package definition
            if(imageInPackage)
                imageInPackage.parentNode.removeChild(imageInPackage)
        }
        //we return, since it contains a language prefix
        //and there's no need to process it
        return
    }

    //logger.info(translationMap[suffixLessFileName])
    //if this image doesn't need to be translated, we return
    if(!c.translationMap[suffixLessFileName]) return

    removeLanguageGears(c.image, c.controllerName)
    addDisplayGear(c.componentDOM, c.image, c.controllerName)
    copyTranslatedImage(c.projectPath, c.imageSourcePath, imagePath, path.basename(fileName), c.assetsQueue, c.logger)

    Object.keys(c.languageSuffixMap).forEach((suffix, suffixIndex) => {
        const imageFileName = c.translationMap[suffixLessFileName][`_${suffix}`]
        if(!imageFileName)
            throw new Error(`Missing '${suffix}' path for '${suffixLessFileName}' in translation map`)
         
        appendTranslatedImageNode(c.image, suffix, suffixIndex, fileNameAttr, imagePath, imageFileName, imageInPackage, c.controllerName)
        copyTranslatedImage(c.projectPath, c.imageSourcePath, imagePath, imageFileName, c.assetsQueue, c.logger)
    })
}