import path = require('path')

export default function getImagePath(image: Element, imageInPackage: Element, packagePath: string, componentPath: string){
    const srcAttr = image.getAttribute('src')
    const fileNameAttr = image.getAttribute('fileName')
    let fileName: string
    //we check the fileName attribute first, since FairyGUI gives it a higher priority over the src attribute, apparently...
    if(typeof fileNameAttr != 'string' || !fileNameAttr.length){
        if(typeof srcAttr != 'string' || !srcAttr.length)
            throw new Error(`Image in ${componentPath} doesn't have a valid 'fileName' or 'src' attribute to match against ${packagePath}.`)

        if(!imageInPackage)
            throw new Error(`Impossible to find a valid source for image with invalid 'fileName' attribute and src '${srcAttr}' in '${componentPath}'. Non-existent element in package.xml`)
            
        const _fileName = imageInPackage.getAttribute('name')
        const _path = imageInPackage.getAttribute('path')
        fileName = path.join(_path, _fileName)
    }
    else{
        fileName = fileNameAttr
    }

    return fileName
}