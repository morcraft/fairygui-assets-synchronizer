import path = require('path')
import getElementsByTagAndAttribute from '../dom/getElementsByTagAndAttribute'

export default function appendTranslatedImageNode(image: Element, suffix: string, suffixIndex: number, fileNameAttr: string, imagePath: string, imageFileName: string, imageInPackage: Element, controllerName: string){
    //const newImage = DOM.createElement('image')
    const clonedImage = image.cloneNode(true) as Element

    //these attributes should change since these pictures aren't defined in package.xml
    clonedImage.setAttribute('id', `${image.getAttribute('id')}_${suffix}`)
    clonedImage.setAttribute('name', `${image.getAttribute('name')}_${suffix}`)

    const targetGear = getElementsByTagAndAttribute(clonedImage, 'gearDisplay', 'controller', controllerName)[0]
    targetGear.setAttribute('pages', (suffixIndex + 1).toString())

    if(typeof fileNameAttr == 'string' && fileNameAttr.length)
        clonedImage.setAttribute('fileName', path.join(imagePath, imageFileName))

    if(imageInPackage){
        clonedImage.setAttribute('src', `${image.getAttribute('src')}_${suffix}`)
        const clonedImageInPackage = imageInPackage.cloneNode(true) as Element
        
        imageInPackage.parentNode.insertBefore(clonedImageInPackage, imageInPackage)
        clonedImageInPackage.setAttribute('id', `${imageInPackage.getAttribute('id')}_${suffix}`)
        clonedImageInPackage.setAttribute('name', imageFileName)
    }

    return image.parentNode.insertBefore(clonedImage, image)
}