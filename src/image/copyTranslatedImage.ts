import path = require('path')
import TaskQueue from '../taskQueue/TaskQueue'
import copyFileAndLog from '../file/copyFileAndLog'

export default function copyTranslatedImage(projectPath: string, imagesSourcePath: string, imagePath: string, imageFileName: string, assetsQueue: TaskQueue, logger){
    const originPath = path.join(imagesSourcePath, imageFileName)
    const targetPath = path.join(projectPath, imagePath, imageFileName)

    //we avoid copying the same asset more than once, 
    //since these may appear multiple times across components
    if(assetsQueue.containsTask(targetPath)) return

    assetsQueue.addTask(targetPath)

    return copyFileAndLog(originPath, targetPath, logger)
    .then(_ => {
        assetsQueue.completeTask(targetPath)
    })
    .catch(error => {
        assetsQueue.completeTask(targetPath)
        throw error
    })
}