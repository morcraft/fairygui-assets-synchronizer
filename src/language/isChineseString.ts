export default function isChineseString(text: string){
    return typeof text == 'string' && text.match(/[\u2E80-\u2FD5\u3190-\u319f\u3400-\u4DBF\u4E00-\u9FCC对]+/gu)
}