export default function(suffixes: string[]){
    return new RegExp(`_(${suffixes.join('|')})`)
}