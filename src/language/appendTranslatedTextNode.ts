import getElementsByTagAndAttribute from '../dom/getElementsByTagAndAttribute'

export default function appendTranslatedTextNode(text: Element, suffix: string, suffixIndex: number, controllerName: string){
    const clonedText = text.cloneNode(true) as Element

    clonedText.setAttribute('id', `${text.getAttribute('id')}_${suffix}`)
    clonedText.setAttribute('name', `${text.getAttribute('name')}_${suffix}`)

    const targetGear = getElementsByTagAndAttribute(clonedText, 'gearDisplay', 'controller', controllerName)[0]
    targetGear.setAttribute('pages', (suffixIndex + 1).toString())

    text.parentNode.insertBefore(clonedText, text)

    return clonedText
}