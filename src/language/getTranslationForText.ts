import Dictionary from '../utils/Dictionary'
import DictionaryCollection from '../utils/DictionaryCollection'
import escapeSpecialCharacters from './escapeSpecialCharacters'

export default function getTranslationForText(text: string, dictionaryCollection: DictionaryCollection, languageMap: Dictionary): Dictionary{
    const translations: Dictionary = {}
    const dictionaryKeys = Object.keys(dictionaryCollection)
    dictionaryKeys.forEach(dictionaryName => {
        const languageSuffix = languageMap[dictionaryName]
        if(typeof languageSuffix != 'string')
            throw new Error(`Couldn't find suffix for language ${dictionaryName}`)

        const dictionary = dictionaryCollection[dictionaryName]

        if(!dictionary.hasOwnProperty(text))
            return true

        if(typeof dictionary[text] != 'string')
            throw new Error(`Invalid type for text ${text} in ${dictionaryName} dictionary. Type: ${typeof dictionary[text]}. Text: ${text}`)

        translations[languageSuffix] = escapeSpecialCharacters(dictionary[text])
    })

    
    const translationKeys = Object.keys(translations)
    //at least one translation is available
    if(translationKeys.length){
        if(translationKeys.length != dictionaryKeys.length)
            throw new Error(`Invalid translation sources. Text ${text} doesn't appear in all the given dictionaries. It appears in ${JSON.stringify(translationKeys)}`)
    }

    return translations
}