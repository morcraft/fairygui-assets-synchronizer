import TranslationMap from './TranslationMap'

export default function getImagesTranslationMap(images: string[], regex: RegExp): TranslationMap{
    const map = {}
    images.forEach(image => {
        const matches = image.match(regex)
        if(!matches)
            return true

        const key = image.replace(matches[0], '')
        if(!map[key]){
            map[key] = { 
                [matches[0]]: image
            }
        }
        else{
            map[key][matches[0]] = image
        }
    })

    return map
}