import getElementsByTagAndAttribute from '../dom/getElementsByTagAndAttribute'
import mapToControllerValues from '../controller/mapToControllerValues'

export default function addLanguageController(componentDOM: Document, component: Element, languageList: string[], controllerName: string){
    const controllers = getElementsByTagAndAttribute(componentDOM, 'controller', 'name', controllerName)
    controllers.forEach(controller => {
        controller.parentNode.removeChild(controller)
    })

    const languageControllerValues = mapToControllerValues(languageList)
    const languageController = componentDOM.createElement('controller')
    languageController.setAttribute('name', controllerName)
    languageController.setAttribute('selected', '0')
    languageController.setAttribute('pages', languageControllerValues)
    
    //equivalent of prepend(), that is, to insert in the first position
    return component.insertBefore(languageController, component.firstChild)
}