const languageSuffixMap = {
    CN2: 'Traditional Chinese',
    EN: 'English',
    IN: 'Indonesian',
    JP: 'Japanese',
    KR: 'Korean',
    TH: 'Thai',
    VN: 'Vietnamese',
}

export default languageSuffixMap