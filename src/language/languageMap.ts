const languageMap = {
    TraditionalChinese: 'CN2',
    English: 'EN',
    Indonesian: 'IN',
    Japanese: 'JP',
    Korean: 'KR',
    Thai: 'TH',
    Vietnamese: 'VN',
}

export default languageMap