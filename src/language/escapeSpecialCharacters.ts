export default function escapeSpecialCharacters(string: string){
    if(typeof string != 'string')
        throw new Error(`Invalid type for given string ${string}`)
    
    return string.replace(/\r/g, '')
}