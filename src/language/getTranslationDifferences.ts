import Dictionary from '../utils/Dictionary'

export default function getTranslationDifferences(imagesToPlace: Dictionary, languageSuffixMap: Dictionary, translationMap: Object, regex: RegExp){
    const filesToCheck = {}

    Object.keys(imagesToPlace).forEach(image => {
        if(!regex.test(image)){//doesn't contain any language suffix
            if(!translationMap[image])
                filesToCheck[image] = []
        }
    })

    Object.keys(translationMap).forEach(key => {
        const missingTranslations = {}
        Object.keys(languageSuffixMap).forEach(suffix => {
            if(!translationMap[key][`_${suffix}`])
                missingTranslations[`_${suffix}`] = true
        })

        const keys = Object.keys(missingTranslations)
        if(keys.length){
            filesToCheck[key] = keys
        }
    })
    return filesToCheck
}