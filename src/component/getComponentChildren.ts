export default function getComponentChildren(component: Element | Document): Element[]{
    const nodes = []
    for(let i = 0; i < component.childNodes.length; i++){
        if(component.childNodes[i].nodeName == 'component')
            nodes.push(component.childNodes[i])
    }

    return nodes
}