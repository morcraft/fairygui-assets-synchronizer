import readFileAndLog from '../file/readFileAndLog'
import getDOM from '../dom/getDOM'
import Dictionary from '../utils/Dictionary'
import { DOMParser, XMLSerializer } from '../index'
import getComponentChildren from './getComponentChildren'
import processImage from '../image/processImage'
import addLanguageController from '../language/addLanguageController'
import onProjectDone from '../utils/onProjectDone'
import writeFileAndLog from '../file/writeFileAndLog'
import TaskQueue from '../taskQueue/TaskQueue'
import TranslationMap from '../language/TranslationMap'

//TODO simplify
export interface FunctionArguments{
    projectPath: string
    componentPath: string
    packagePath: string
    packageDOM: Document
    componentsQueue: TaskQueue
    assetsQueue: TaskQueue
    languageSuffixMap: Dictionary
    languageSuffixes: string[]
    languageSuffixRegex: RegExp
    translationMap: TranslationMap
    imageSourcePath: string
    controllerName: string
    logger
}

export default function processComponent(c: FunctionArguments){
    c.componentsQueue.addTask(c.componentPath)
    return readFileAndLog(c.componentPath, c.logger)
    .then(componentBuffer => {
        const componentDOM = getDOM(DOMParser, componentBuffer)
        const componentChildren = getComponentChildren(componentDOM)
        if(!Array.isArray(componentChildren) || !componentChildren.length){
            c.componentsQueue.completeTask(c.componentPath, (_ => { onProjectDone(c.packagePath, c.packageDOM, c.logger) }))
            return true
        }

        const component = componentChildren[0]
        const images = component.getElementsByTagName('image')
        const imagesLength = images.length //don't even attempt to remove this
        for(let i = 0; i < imagesLength; i++){
            const image = images[i]
            processImage({
                image,
                projectPath: c.projectPath,
                packageDOM: c.packageDOM,
                packagePath: c.packagePath,
                componentDOM,
                componentPath: c.componentPath,
                assetsQueue: c.assetsQueue,
                languageSuffixMap: c.languageSuffixMap,
                languageSuffixRegex: c.languageSuffixRegex,
                translationMap: c.translationMap,
                imageSourcePath: c.imageSourcePath,
                controllerName: c.controllerName,
                logger: c.logger,
            })
        }

        addLanguageController(componentDOM, component, c.languageSuffixes, c.controllerName)

        c.componentsQueue.completeTask(c.componentPath, (_ => { onProjectDone(c.packagePath, c.packageDOM, c.logger) }))

        const componentXMLString = XMLSerializer.serializeToString(componentDOM)

        writeFileAndLog(c.componentPath, componentXMLString, c.logger)
    })
}