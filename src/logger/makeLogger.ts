const simpleNodeLogger = require('simple-node-logger')

export default function makeLogger(options: Object){
    return simpleNodeLogger.createSimpleLogger(options)
}