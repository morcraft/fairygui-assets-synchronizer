import path = require('path')

const loggerOptions = {
    logFilePath: path.join(process.cwd(), `fairyGUIProject${Date.now()}.log`),
    timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'
}

export default loggerOptions