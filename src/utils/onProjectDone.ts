const chalk = require('chalk')
const pretty = require('pretty')
import fs = require('fs-extra')
import { XMLSerializer } from '../index'

export default function onProjectDone(packagePath: string, packageDOM: Document, logger){
    logger.info(chalk.green(`Finished processing ${packagePath}`))
    const packageString = XMLSerializer.serializeToString(packageDOM)
    fs.outputFile(packagePath, pretty(packageString))
    .then(_ => {
        logger.info(`${chalk.bgGreen(packagePath)} ✔`)
    })
    .catch(error => {
        logger.error(chalk.red(error))
        throw error
    })
}