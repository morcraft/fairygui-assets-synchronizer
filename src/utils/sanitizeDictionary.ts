import Dictionary from './Dictionary'
import escapeSpecialCharacters from '../language/escapeSpecialCharacters'

export default function sanitizeDictionary(dictionary: Dictionary){
    const map = {}
    Object.keys(dictionary).forEach(key => {
        map[escapeSpecialCharacters(key)] = dictionary[key]
    })

    return map
}