import Dictionary from './Dictionary'

export default function arrayToMap(array: string[]): Dictionary{
    const map = {}
    array.forEach(element => {
        if(map[element])
            throw new Error(`Key ${element} appears more than once in the collection.`)
        map[element] = element
    })
    return map
}