export default function addDisplayGear(componentDOM: Document, element: Element, controllerName: string){
    const gear = componentDOM.createElement('gearDisplay')
    gear.setAttribute('controller', controllerName)
    gear.setAttribute('pages', '0')
    element.appendChild(gear)
    return gear
}
