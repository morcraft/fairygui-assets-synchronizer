import { setGearValue } from './attributeToGearValue'

export default function addLookGear(componentDOM: Document, element: Element, controllerName: string){
    const gear = componentDOM.createElement('gearLook')
    gear.setAttribute('controller', controllerName)
    const alpha = setGearValue(element, 'alpha')
    const rotation = setGearValue(element, 'rotation')
    const grayed = setGearValue(element, 'grayed')
    const touchable = setGearValue(element, 'touchable')
    gear.setAttribute('default', `${alpha},${rotation},${grayed},${touchable}`)
    element.appendChild(gear)
    return gear
}