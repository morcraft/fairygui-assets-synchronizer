import { setGearValue } from './attributeToGearValue'

export default function addGXYear(componentDOM: Document, element: Element, controllerName: string){
    const gear = componentDOM.createElement('gearXY')
    gear.setAttribute('controller', controllerName)
    gear.setAttribute('default', setGearValue(element, 'xy'))
    element.appendChild(gear)
    return gear
}
