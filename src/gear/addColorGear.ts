import { setGearValue } from './attributeToGearValue'

export default function addColorGear(componentDOM: Document, element: Element, controllerName: string){
    const gear = componentDOM.createElement('gearColor')
    gear.setAttribute('controller', controllerName)
    const color = setGearValue(element, 'color')
    gear.setAttribute('default', color)
    element.appendChild(gear)
    return gear
}