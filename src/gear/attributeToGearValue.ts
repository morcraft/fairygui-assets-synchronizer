//This module contains the default value implementations for FairyGUI gear elements

//GearLook = Alpha/Rotation/Grayed/Touchable
export const alpha = '1'
export const rotation = '0'
export const grayed = '0'
export const touchable = '1'

//GearSize = Width/Height/ScaleX/ScaleY
export const size = '1,1'
export const scale = '1,1'

//GearColor
export const color = '#ffffff'

//GearXY
export const xy = '1,1'

//GearText
export const text = ''

export const map = {
    alpha, rotation, grayed, touchable, size, scale, color, xy, text
}

export type Gear = keyof typeof map

export function setGearValue(element: Element, attribute: Gear): string{
    const value =  element.getAttribute(attribute)
    //touchable/invisible/grayed
    if(value == 'true') return '1'
    if(value == 'false') return '0'
    if(typeof value == 'string' && value.length) return value
    return map[attribute]
}