import { setGearValue } from './attributeToGearValue'

export default function addTextYear(componentDOM: Document, element: Element, controllerName: string){
    const gear = componentDOM.createElement('gearText')
    gear.setAttribute('controller', controllerName)
    gear.setAttribute('default', setGearValue(element, 'text'))
    element.appendChild(gear)
    return gear
}
