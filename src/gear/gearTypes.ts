const gearTypes = {
    gearColor: true,
    gearDisplay: true,
    gearLook: true,
    gearSize: true,
    gearXY: true,
    gearText: true,
}

export default gearTypes