import getElementsByTagAndAttribute from '../dom/getElementsByTagAndAttribute'
import gearTypes from './gearTypes'

export default function removeLanguageGears(element: Element, controllerName: string){
    Object.keys(gearTypes).forEach(gearType => {
        const displayElements = getElementsByTagAndAttribute(element, gearType, 'controller', controllerName)
        displayElements.forEach(element => {
            element.parentNode.removeChild(element)
        })
    })
}