import addColorGear from './addColorGear'
import addDisplayGear from './addDisplayGear'
import addLookGear from './addLookGear'
import addSizeGear from './addSizeGear'
import addXYGear from './addXYGear'

export interface Gears{
    colorGear: HTMLElement
    lookGear: HTMLElement
    sizeGear: HTMLElement
    XYGear: HTMLElement
    displayGear?: HTMLElement
}

export default function addLanguageGears(componentDOM: Document, element: Element, logger, controllerName: string, imageSourcePath?: string){
    const gears: Gears = {
        colorGear: addColorGear(componentDOM, element, controllerName),
        lookGear: addLookGear(componentDOM, element, controllerName),
        sizeGear: addSizeGear(componentDOM, element, controllerName, logger, imageSourcePath),
        XYGear: addXYGear(componentDOM, element, controllerName),
    }

    //text elements' visibility shouldn't change according to the controller's page
    if(element.tagName.toLowerCase() != 'text'){
        gears.displayGear = addDisplayGear(componentDOM, element, controllerName)
    }

    return gears
}
