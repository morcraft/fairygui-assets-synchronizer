import * as attributeToGearValue from './attributeToGearValue'
import imageSize = require('image-size')

export default function addSizeGear(componentDOM: Document, element: Element, controllerName: string, logger, imageSourcePath?: string){
    //we need imageSourcePath here, because if the image doesn't have a set width and height, FairyGUI will add the picture size
    //as a default value in the SizeGear, so we need to get the real value in pixels to set it in case
    //it doesn't have one as an attribute

    const gear = componentDOM.createElement('gearSize')
    gear.setAttribute('controller', controllerName)
    
    let size = element.getAttribute('size')
    if(typeof size != 'string' || !size.length){
        try{
            if(element.tagName.toLowerCase() == 'image'){
                if(!imageSourcePath)
                    throw new Error(`An image source path is needed for image node`)
    
                const realSize = imageSize(imageSourcePath)
                size = `${realSize.width},${realSize.height}`
            }
            else{
                size = attributeToGearValue.size
            }
        }
        catch(error){
            logger.error(`Error while getting size of ${imageSourcePath} ${error}`)
            throw error
        }
    }

    const scale = attributeToGearValue.setGearValue(element, 'scale')
    gear.setAttribute('default', `${size},${scale}`)
    element.appendChild(gear)
    return gear
}