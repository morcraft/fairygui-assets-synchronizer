const chalk = require('chalk')
import fs = require('fs-extra')

export default function copyFileAndLog(originPath: string, targetPath: string, logger){
    return fs.copy(originPath, targetPath)
    .then(_ => {
        logger.info(`${chalk.green(originPath)} => ${chalk.green(targetPath)} ✔`)
    })
    .catch(error => {
        logger.error(`${chalk.red(originPath)} => ${chalk.red(targetPath)}`, error)
    })
}