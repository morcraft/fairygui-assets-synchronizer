export default function getFileExtension(filePath: string){
    const lastDotPos = filePath.lastIndexOf('.')
    if(lastDotPos < 0) return ''
    if(lastDotPos == filePath.length - 1) return ''
    return filePath.substr(lastDotPos + 1)
}