const chalk = require('chalk')
import fs = require('fs-extra')

export default function writeFileAndLog(targetPath: string, string: string, logger){
    return fs.outputFile(targetPath, string)
    .then(_ => {
        logger.info(`${chalk.cyan(targetPath)} ✔`)
    })
    .catch(error => {
        logger.error(`Error while writing file ${targetPath} ${chalk.red(error)}`)
        throw error
    })
}