import getFileExtension from './getFileExtension'

export default function filterFiles(assets: string[], extensions: string[]): string[]{
    const images = []
    assets.forEach(asset => {
        const extension = getFileExtension(asset)
        if(extensions.includes(extension))
            images.push(asset)
    })

    return images
}