import recursiveReadDir = require('recursive-readdir')
import filterFiles from './filterFiles'

export default function getFilesByExtensions(directory: string, extensions: string[]): Promise<string[]>{
    const content = recursiveReadDir(directory)
    return content.then(files => {
        return filterFiles(files, extensions)
    },
    error => {
        throw error
    })
}