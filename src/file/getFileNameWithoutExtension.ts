import getFileExtension from './getFileExtension'

export default function getFileNameWithoutExtension(filePath: string){
    const extension = getFileExtension(filePath)
    if(extension.length)
        return filePath.split(`.${extension}`).slice(0, -1).join('')
    
    return filePath
}