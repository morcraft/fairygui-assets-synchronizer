import fs = require('fs-extra')

export default function readFileAndLog(filePath: string, logger){
    return fs.readFile(filePath)
    .catch(error => {
        logger.error(`Error while reading file ${filePath}`, error)
        throw error
    })
}