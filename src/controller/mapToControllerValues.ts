export default function mapToControllerValues(map: string[]){
    //TODO map.reduce
    let string = ''
    map.forEach((key, index) => {
        string += `${index},${key}` + (index < map.length - 1 ? ',' : '')
    })
    return string
}